﻿using Site_1.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site_1.ViewModel
{
    public class DesertsListViewModel
    {
        public IEnumerable<Desert> allDeserts { get; set; }
        public string currCategory { get; set; }
    }
}
