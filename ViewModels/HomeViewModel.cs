﻿using Site_1.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site_1.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Desert> favDeserts { get; set; }
    }
}
