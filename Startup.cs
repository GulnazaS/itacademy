using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Site_1.Data;
using Site_1.Data.interfaces;
using Site_1.Data.Models;
using Site_1.Data.Repository;
using System.IO;

namespace Site_1
{
    public class Startup
    {
        private IConfigurationRoot _confstring;
        public Startup(IWebHostEnvironment hostEnv)
        {
            _confstring = new ConfigurationBuilder().
                SetBasePath(hostEnv.ContentRootPath).AddJsonFile("dbsettings.json").Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDBContent>
                (options => options.UseSqlServer(_confstring.GetConnectionString("DefaultConnection")));
            
            services.AddDataProtection().PersistKeysToFileSystem(new DirectoryInfo("DataProtectionKeys"));
            services.AddTransient<IAllDeserts, DesertRepository>();
            services.AddTransient<IDesertsCategory, CategoryRepository>();
            services.AddTransient<IAllOrders, OrdersRepository>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>(); 
            services.AddScoped(sp => ShopCart.GetCart(sp));                     
            services.AddMvc();
            services.AddMemoryCache(); 
            services.AddSession();  

        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(name: "categoryFilter", "Desert/{action}/{category?}",
                    defaults: new { Controller = "Desert", action = "List" });
            });

            using (var scoupe = app.ApplicationServices.CreateScope())
            {
                AppDBContent content = scoupe.ServiceProvider.GetRequiredService<AppDBContent>();
                DBObjects.Initial(content);
            }
        }
    }
}
