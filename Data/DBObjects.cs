﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Site_1.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site_1.Data
{
    public class DBObjects
    {
        public static void Initial(AppDBContent content)
        {
           
            if (!content.Category.Any())
                content.Category.AddRange(Categories.Select(c => c.Value));

            if (!content.Desert.Any())
            {
                content.AttachRange(
                    new Desert
                    {
                        name = "Торт Медовый",
                        shortDesc = "Бисквитный и вкусный",
                        longDesc = "Доподлинно не известно, кто и когда впервые создал рецепт этого торта, но практически каждый знаком с его замечательным вкусом. Воздушные медовые коржи и нежный сметанный крем с добавлением вареной сгущенки составляют идеальный ансамбль, а перед сладким ароматом цветочного меда, который используется при приготовлении, невозможно устоять!",
                        img = "/img/medovik.jpg",
                        price = 1000,
                        isFavourite = true,
                        available = true,
                        Category = Categories["Торты"]
                    },
                    new Desert
                    {
                        name = "Наполеон",
                        shortDesc = "Бисквитный кремовый торт",
                        longDesc = "Торт наполеон — старый и проверенный рецепт. Торт наполеон классический, наполеон торт из слоеного теста, рецепт торт наполеон на пиве, торт наполеон со сгущенкой, торт наполеон с заварным кремом, торт мокрый наполеон — у вас есть масса вариантов, как сделать торт наполеон, и в любом случае приготовление торта наполеон окупится сторицей.",
                        img = "/img/napoleon.png",
                        price = 1000,
                        isFavourite = true,
                        available = true,
                        Category = Categories["Торты"]
                    },
                    new Desert
                    {
                        name = "Пирожное Картошка",
                        shortDesc = "Из печенья и сгущенки",
                        longDesc = "Пирожное картошка — рецепт родом из нашего детства. Приготовление пирожного картошка не требовало много продуктов, поэтому пирожное картошка часто готовили в дефицитные советские времена. Есть несколько основных вариантов, как готовить пирожное картошка, это пирожное картошка из бисквита, пирожное картошка из печенья, рецепт пирожное картошка из сухарей.",
                        img = "/img/potato.jpg",
                        price = 50,
                        isFavourite = false,
                        available = true,
                        Category = Categories["Пирожные"]
                    },
                    new Desert
                    {
                        name = "Муссовое шоколадное пирожное",
                        shortDesc = "Воздушное пирожное",
                        longDesc = "Муссовое пирожное - это всегда беспроигрышный вариант для угощения. Получается красиво, готовить не сложно. Да, по времени затратно, но это того стоит. Каждый новый слой готовится из разного вида шоколада, получается очень гармонично. Украсить пирожное можно чем угодно: кондитерской посыпкой, ягодами, шоколадной стружкой.",
                        img = "/img/sugar.jpg",
                        price = 200,
                        isFavourite = false,
                        available = true,
                        Category = Categories["Пирожные"]
                    },
                    new Desert
                    {
                        name = "Пончики с глазурью",
                        shortDesc = "Выпечка из дрожжевого теста ",
                        longDesc = "Сладкая пышка, политая сладкой глазурью, - это про донатс, он же пончик.Как только его не называют в разных странах мира! Например, в Израиле пончики именуются sufganiyah.На Украине их величают пампушками, в Польше – pączki, а в Боснии и Герцеговине эта выпечка называется krofne.",
                        img = "/img/donut.jpg",
                        price = 150,
                        isFavourite = true,
                        available = true,
                        Category = Categories["Выпечка"]
                    },
                    new Desert
                    {
                        name = "Пирожок с яблоками",
                        shortDesc = "Из слоеного теста с корицей",
                        longDesc = "Румяные, мягкие пирожки с яблоками и изюмом из дрожжевого теста. Яблочные пирожки с корицей – классический вариант сладкой сдобной выпечки. Добавление сухофруктов придает выпечке дополнительную сладость и насыщенный аромат.",
                        img = "/img/apple.jpg",
                        price = 20,
                        isFavourite = true,
                        available = true,
                        Category = Categories["Выпечка"]
                    },
                    new Desert
                    {
                        name = "Пончики в глазури",
                        shortDesc = "С начинкой",
                        longDesc = "Румяные, мягкие пирожки с яблоками и изюмом из дрожжевого теста. Яблочные пирожки с корицей – классический вариант сладкой сдобной выпечки. Добавление сухофруктов придает выпечке дополнительную сладость и насыщенный аромат.",
                        img = "/img/apple.jpg",
                        price = 20,
                        isFavourite = true,
                        available = true,
                        Category = Categories["Пончики"]
                    }
                    );
            }

            content.SaveChanges();

        }

        

        private static Dictionary<string, Category> category;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (category == null)
                {
                    var list = new Category[]
                    {
                         new Category{categoryName = "Торты", desc = "Бисквитный и вкусный"},
                         new Category{categoryName = "Выпечка", desc = "Из слоеного теста с корицей"},
                         new Category {categoryName = "Пирожные", desc = "Пирожное Картошка"},
                         new Category {categoryName = "Пончики", desc = "С начинкой"}
                    };

                    category = new Dictionary<string, Category>();
                    foreach (Category el in list)
                        category.Add(el.categoryName, el);
                }
                return category;
            }
        }
    }
}

