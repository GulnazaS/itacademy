﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site_1.Data.Models
{
    public class OrderDetail
    {
        public int id { get; set; }
        public int orderID { get; set; }
        public int DesertID { get; set; }
        public uint price { get; set; }
        public virtual Desert desert { get; set; }
        public virtual Order order { get; set; }
    }
}