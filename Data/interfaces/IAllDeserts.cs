﻿using Site_1.Data.Models;
using System.Collections.Generic;

namespace Site_1.Data.interfaces
{
    public interface IAllDeserts
    {
        IEnumerable<Desert> Deserts { get; }
        IEnumerable<Desert> getFavDeserts { get; }
        Desert getObjectDesert(int desertId);
    }
}
