﻿using Microsoft.EntityFrameworkCore;
using Site_1.Data.interfaces;
using Site_1.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site_1.Data.Repository
{
    public class DesertRepository : IAllDeserts
    {
        private readonly AppDBContent _appDbContent;
        public DesertRepository(AppDBContent appDbContent)
        {
            this._appDbContent = appDbContent;
        }
        public IEnumerable<Desert> Deserts => _appDbContent.Desert.Include(c => c.Category);
        public IEnumerable<Desert> getFavDeserts => _appDbContent.Desert.Where(p => p.isFavourite).Include(c => c.Category);
        public Desert getObjectCar(int desertId) => _appDbContent.Desert.FirstOrDefault(p => p.id == desertId);
        public Desert getObjectDesert(int desertId)
        {
            throw new NotImplementedException();
        }
    }
}
