﻿using Microsoft.AspNetCore.Mvc;
using Site_1.Data.interfaces;
using Site_1.Data.Models;
using Site_1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site_1.Controllers
{
    public class ShopCartController : Controller
    {
        private readonly IAllDeserts _desertRep;
        private readonly ShopCart _shopCart;
        //internal object listShopItems;
        public ShopCartController(IAllDeserts desertRep, ShopCart shopCart)
        {
            _desertRep = desertRep;
            _shopCart = shopCart;
        }
        public ViewResult Index()
        {
            var items = _shopCart.getShopItems();
            _shopCart.listShopItems = items;

            var obj = new ShopCartViewModel
            {
                shopCart = _shopCart
            };
            return View(obj);
        }

        /*internal object getShopItems()
        {
            throw new NotImplementedException();
        }*/
        public RedirectToActionResult addToCart(int id)
        {
            var item = _desertRep.Deserts.FirstOrDefault(i => i.id == id);
            if (item != null)
            {
                _shopCart.AddToCart(item);
            }
            return RedirectToAction("Index");
        }
        //public static implicit operator ShopCartController(ShopCart v)
        //{
       //     throw new NotImplementedException();
       // }
    }
}
