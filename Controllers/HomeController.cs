﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Site_1.Data;
using Site_1.Data.interfaces;
using Site_1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site_1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAllDeserts _desertRep;
        private readonly AppDBContent _db;
        public HomeController(IAllDeserts desertRep, AppDBContent db)
        {
            _desertRep = desertRep;
            _db = db;
        }
        [HttpGet]
        public async Task<IActionResult> Index(string Empsearch)
        {
            ViewData["Getemployeedetails"] = Empsearch;

            var empquery = from x in _db.Desert select x;
            if (!String.IsNullOrEmpty(Empsearch))
            {
                empquery = empquery.Where(x => x.name.Contains(Empsearch) || x.Category.categoryName.Contains(Empsearch));
            }
            var homeDeserts = new HomeViewModel
            {
                favDeserts = await empquery.AsNoTracking().ToListAsync()
            };
            return View(homeDeserts);
        }
        public ViewResult Index()
        {
            var homeDeserts = new HomeViewModel
            {
                favDeserts = _desertRep.getFavDeserts
            };
            return View(homeDeserts);
        }
    }
}
