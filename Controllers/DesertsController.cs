﻿using Microsoft.AspNetCore.Mvc;
using Site_1.Data.interfaces;
using Site_1.Data.Models;
using Site_1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Site_1.Controllers
{
    public class DesertsController : Controller
    {
        private readonly IAllDeserts _allDeserts;
        private readonly IDesertsCategory _allCategories;
        public DesertsController(IAllDeserts iAllDeserts, IDesertsCategory iDesertsCat)
        {
            _allDeserts = iAllDeserts;
            _allCategories = iDesertsCat;
               
        }
        [Route("Deserts/List")]
        [Route("Deserts/List/{category}")]
        public ViewResult List(string category)
        {
            IEnumerable<Desert> deserts = null;
            string currСategory = "";
            if (string.IsNullOrEmpty(category))
            {
                deserts = _allDeserts.Deserts.OrderBy(i => i.id);
            }
            else
            {
                if (string.Equals("Пирожные", category, StringComparison.OrdinalIgnoreCase))
                {
                    deserts = _allDeserts.Deserts.Where(i => i.Category.categoryName.Equals("Пирожные")).OrderBy(i => i.id);
                    currСategory = "Пирожные";
                }
                else if (string.Equals("Выпечка", category, StringComparison.OrdinalIgnoreCase))
                {
                    deserts = _allDeserts.Deserts.Where(i => i.Category.categoryName.Equals("Выпечка")).OrderBy(i => i.id);
                    currСategory = "Выпечка";
                }
                else if (string.Equals("Торты", category, StringComparison.OrdinalIgnoreCase))
                {
                    deserts = _allDeserts.Deserts.Where(i => i.Category.categoryName.Equals("Торты")).OrderBy(i => i.id);
                    currСategory = "Торты";
                }
                else if (string.Equals("Пончики", category, StringComparison.OrdinalIgnoreCase))
                {
                    deserts = _allDeserts.Deserts.Where(i => i.Category.categoryName.Equals("Пончики")).OrderBy(i => i.id);
                    currСategory = "Пончики";
                }
            }
            var desertObj = new DesertsListViewModel
            {
                allDeserts = deserts,
                currCategory = currСategory
            };

            ViewBag.title = "Страница о кондитерских изделиях";
            return View(desertObj);
        }
    }

    
}
